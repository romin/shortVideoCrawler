package com.romin.action;

import com.romin.dao.ManagerDao;
import com.romin.pojo.Manager;
import com.romin.pojo.ResponseBean;
import org.jboss.logging.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author cloud.eve
 */
@Controller
public class AdminAction {
    @Resource
    public ManagerDao managerDao;

    @RequestMapping("/admin")
    public String adminAction() {
        return "main";
    }

    @RequestMapping("/admin/tpl/{tplName}")
    public String helloAction1(@PathVariable String tplName) {
        return "tpl/" + tplName;
    }

    @RequestMapping("/admin/views/{tplName}")
    public String viewTemplate(@PathVariable String tplName) {
        return "views/" + tplName;
    }

    @RequestMapping("/admin/views/{tplName}/{tplName2}")
    public String viewTemplate2(@PathVariable String tplName, @PathVariable String tplName2) {
        return "views/" + tplName + "/" + tplName2;
    }

    @RequestMapping("/admin/views/userManager")
    public String userManager(Model model) {
        Iterable<Manager> managers = managerDao.findAll();
        model.addAttribute("managers", managers);
        return "views/userManager";
    }

    @RequestMapping(value = "/admin/updateUser",method = RequestMethod.POST)
    @ResponseBody
    public String updateuserManager(@Param Manager manager) {
        Manager originManager = null;
        boolean editFlag = false;
        if (manager.getId() != null) {
            originManager = managerDao.findOne(manager.getId());
        }
        if (manager.getStatus() != null) {
            editFlag = true;
            originManager.setStatus(manager.getStatus());
        }
        if (manager.getAccessType() != null) {
            editFlag = true;
            originManager.setAccessType(manager.getAccessType());
        }
        boolean result = false;
        try {
            if (editFlag) {
                managerDao.save(originManager);
                result = true;
            }
        } catch (Exception e) {
        }
        return new ResponseBean(result).build();
    }


}
