package com.romin.action;

import com.romin.dao.AdvertisementDao;
import com.romin.pojo.Advertisement;
import com.romin.pojo.ResponseBean;
import org.jboss.logging.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author cloud.eve
 */
@Controller
public class AdvertisementAction {
    @Resource
    public AdvertisementDao advertisementDao;

    @RequestMapping("/admin/views/advertisementManager")
    public String userManager(Model model) {
        Iterable<Advertisement> managers = advertisementDao.findAll();
        model.addAttribute("advertisement", managers);
        return "views/advertisementManager";
    }

    @RequestMapping(value = "/admin/updateAdvertisement",method = RequestMethod.POST)
    @ResponseBody
    public String updateuserManager(@Param Advertisement advertisement) {
        Advertisement originAdvertisement1 = null;
        boolean editFlag = false;
        if (advertisement.getId() != null) {
            originAdvertisement1 = advertisementDao.findOne(advertisement.getId());
        }
        boolean result = false;
        try {
            if (editFlag) {
                advertisementDao.save(originAdvertisement1);
                result = true;
            }
        } catch (Exception e) {
        }
        return new ResponseBean(result).build();
    }


}
