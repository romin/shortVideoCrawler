package com.romin.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author cloud.eve
 */
@Controller
public class LoginAction {

    @RequestMapping("/login")
    public String login(Map<String, Object> model) {
        return "login";
    }
}
