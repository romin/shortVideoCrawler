package com.romin.crawler;

import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.ExtractByUrl;
import us.codecraft.webmagic.model.annotation.HelpUrl;
import us.codecraft.webmagic.model.annotation.TargetUrl;

/**
 * @author cloud.eve
 */
@TargetUrl("http://www.meipai.com/media/\\w+")
@HelpUrl("http://www.meipai.com/square/\\w+")
public class MeipaiCrawler {
    @ExtractBy(value = "//h3[@class='detail-name']/a[@class='detail-name-a']/text()")
    private String userName;

    @ExtractBy(value = "//h3[@class='detail-name']/a[@class='detail-name-a']/@href")
    private String userId;

    @ExtractBy(value = "//h1[@class='detail-description']/text()")
    private String desc;

    @ExtractByUrl(value = "http://www.meipai.com/media/(\\w+)")
    private String vid;

    @ExtractBy(value = "//div[@class='detai-crumbs']/a[2]/text()")
    private String category;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
