package com.romin.dao;

import com.romin.pojo.Advertisement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author cloud.eve
 */
@RepositoryRestResource(exported = true)
public interface AdvertisementDao extends CrudRepository<Advertisement, Long> {
}
