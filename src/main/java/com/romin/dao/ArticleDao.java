package com.romin.dao;

import com.romin.pojo.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * @author cloud.eve
 */
@RepositoryRestResource(exported = true)
public interface ArticleDao extends CrudRepository<Article, Long> {

    @RestResource(exported = true,path = "findById",rel = "findById")
    public Article findById(@Param("id") Integer id);
}
