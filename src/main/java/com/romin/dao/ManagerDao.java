package com.romin.dao;

import com.romin.pojo.Manager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * @author cloud.eve
 */
@RepositoryRestResource(exported = true)
public interface ManagerDao extends CrudRepository<Manager, Long> {
    Manager findByUserName(@Param("userName") String userName);

    @RestResource(path = "userNameStartsWith", rel = "userNameStartsWith")
    public Page findByUserNameStartsWith(@Param("userName") String userName, Pageable p);
}
