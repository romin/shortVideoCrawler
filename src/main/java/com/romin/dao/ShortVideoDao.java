package com.romin.dao;

import com.romin.pojo.ShortVideo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * @author cloud.eve
 */
public interface ShortVideoDao extends PagingAndSortingRepository<ShortVideo, Long> {
    @RestResource(exported = true,path = "findByVid",rel = "findByVid")
    public ShortVideo findById(@Param("vid") Integer vid);
}
