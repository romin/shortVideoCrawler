package com.romin.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author cloud.eve
 * 广告
 */
@Entity
public class Advertisement {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * 广告的名称
     */
    private String name;

    /**
     * 广告类型 1:文字，2:图片
     */
    private int type = 1;

    /**
     * 广告图片
     */
    private String image;

    /**
     * 广告文字描述
     */
    private String desc;

    /**
     *点击广告跳转连接
     */
    private String targetUrl;

    /**
     * 该条广告的备注
     */
    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
