package com.romin.pojo;

import javax.persistence.*;

/**
 * @author cloud.eve
 * 文章，或者页面
 */
@Entity
public class Article {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 文章内容
     */
    @Lob
    private String content;

    /**
     * 文章分类
     * 这个可以以后再实现
     */
    private String category;

    /**
     * 该文章绑定的广告
     */
    @OneToOne
    private Advertisement advertisement;

    /**
     * 广告形式，广告位置
     * 1:底部，2:顶部
     */
    private Integer adsLocation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    public Integer getAdsLocation() {
        return adsLocation;
    }

    public void setAdsLocation(Integer adsLocation) {
        this.adsLocation = adsLocation;
    }
}
