package com.romin.pojo;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author cloud.eve
 */

@Entity
public class Manager {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    public static int ACCESS_TYPE_ADMIN = 1;
    public static int ACCESS_TYPE_MANAGER = 2;
    public static int ACCESS_TYPE_COMMON = 3;
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户密码
     * 经过bcrypt加密
     */
    @RestResource(exported = false)
    private String password;

    /**
     * 用户权限 0：普通用户 1：代理商 2：管理员
     */
    private Integer accessType;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 手机
     */
    private String telephone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态
     */
    private Integer status;


    protected Manager() {
    }

    public Manager(String userName, String password) {
        this(userName, password, 1);
    }

    public Manager(String userName, String password, int accessType) {

        this.userName = userName;
        this.setPassword(password);
        this.accessType = accessType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
