package com.romin.pojo;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by luozm on 2016/5/1.
 */
public class ResponseBean {
    //暂定于100表示成功，200表示失败
    private int status;
    private Object data;

    public ResponseBean(boolean success) {
        if (success)
            status = 100;
        else
            status = 200;
    }

    public ResponseBean withStatus(int status) {
        this.status = status;
        return this;
    }

    public ResponseBean withData(Object data) {
        this.data = data;
        return this;
    }

    public String build() {
        return JSONObject.toJSONString(this);
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
