package com.romin.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cloud.eve
 */

@RestController
public class LoginRestAction {
    @RequestMapping("/rest/login")
    public String login() {
        return "login";
    }
}
