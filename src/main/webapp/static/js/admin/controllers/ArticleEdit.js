'use strict';

MetronicApp.controller('articleEdit', function ($rootScope, $scope, $http, $timeout, $stateParams,$state) {
    $scope.$on('$viewContentLoaded', function () {
        Metronic.initAjax(); // initialize core components
        Layout.setSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile')); // set profile link active in sidebar menu 
    });

    if ($stateParams.id) {
        $http.get("/api/articles/" + $stateParams.id)
            .success(function (response) {
                $scope.articls = response;
                $('#summernote_1').code(response.content);
            });
    }
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.processSubmit = function () {

        var art = $scope.articls;
        if(!art){
            art={
                "title":$("#article_title").val()
            }
        }
        art.content = $('#summernote_1').code();
        $http({
            method: 'POST',
            url: '/admin/articleUpdate',
            data: $.param(art),  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
        }).success(function(data)
        {
            //$location.path('/articleManager').replace();
            //$route.load('/articleManager');
            $state.go("articleManager",{}, {reload: true});
        });
    }

}); 
