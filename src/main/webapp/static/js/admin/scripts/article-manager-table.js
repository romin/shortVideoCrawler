



var TableAdvanced = function () {

    var initTable1 = function () {
        var table = $('#articleManagerTable');

        /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

        /* Set tabletools buttons and button container */

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-btn-group",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            }

        });

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            //"order": [
            //    [0, 'asc']
            //],
            
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5

            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });

        oTable.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        oTable.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        var nEditing = null;
        var nNew = false;

        oTable.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "保存") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            //jqTds[4].innerHTML = '<input type="select" class="form-control input-small" value="' + aData[4] + '">';

            var $ads_model= $(".advertisement_model").clone();
            $ads_model.find("option[val_dsc='"+aData[1]+"']").attr("selected",true);
            jqTds[1].innerHTML = $ads_model.removeClass("advertisement_model").removeClass("hidden")[0].outerHTML;
            //jqTds[5].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[5] + '">';
            //jqTds[5].innerHTML = '<select class="form-control input-sm"><option value="0" val_dsc="正常">正常</option> <option value="1" val_dsc="屏蔽">屏蔽</option></select>';
            //$(jqTds[5]).find("option[val_dsc='"+aData[5]+"']").attr("selected",true);
            jqTds[2].innerHTML = '<select class="form-control input-sm"><option value="0" val_dsc="无">无</option> <option value="1" val_dsc="底部">底部</option> <option value="2" val_dsc="顶部">顶部</option></select>';
            $(jqTds[2]).find("option[val_dsc='"+aData[2]+"']").attr("selected",true);
            jqTds[3].innerHTML = '<a class="edit" href="">保存</a>&nbsp;<a class="cancel" href="">取消</a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('.form-control', nRow);
            $.post("/admin/articleUpdate",{"id":$(nRow).find("td[manager_id]").attr("manager_id"),"advertisement.id":$(jqInputs[0]).find("option:selected").val(),"adsLocation":$(jqInputs[1]).find("option:selected").val()},function(resp){
                console.log($.parseJSON(resp).status);
                if($.parseJSON(resp).status ==100){
                    oTable.fnUpdate($(jqInputs[0]).find("option:selected").attr("val_dsc"), nRow, 1, false);
                    oTable.fnUpdate($(jqInputs[1]).find("option:selected").attr("val_dsc"), nRow, 2, false);
                    oTable.fnUpdate('<a class="edit" href="javascript:;">编辑</a>', nRow, 6, false);
                    oTable.fnDraw();
                }else{
                    alert("操作失败!");
                }
            });
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            oTable.fnDraw();
        }

        //var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        //
        //tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }


    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }

    };

}();