<div class="page-sidebar navbar-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->
	<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
	<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
	<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
	<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
		<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
		<li class="sidebar-search-wrapper">
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
			<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
			<form class="sidebar-search sidebar-search-bordered" action="extra_search.html" method="POST">
				<a href="javascript:;" class="remove">
				<i class="icon-close"></i>
				</a>
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form>
			<!-- END RESPONSIVE QUICK SEARCH FORM -->
		</li>
        <li class="start active open">
            <a href="javascript:;">
                <i class="icon-home"></i>
                <span class="title">文章系统</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <li class="active">
                    <a href="#/articleManager">
                        <i class="icon-bar-chart"></i>
                        我的文章</a>
                </li>
                <li class="active">
                    <a href="#/articleEdit/">
                        <i class="icon-bar-chart"></i>新的文章</a>
                </li>
                <li>
                    <a href="#/articleModel">
                        <i class="icon-bulb"></i>
                        线上模板</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;">
                <i class="icon-present"></i>
                <span class="title">广告系统</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="#/advertisementManager">
                        我的广告</a>
                </li>
                <li>
                    <a href="#/advertisementModel">
                        广告模板</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;">
                <i class="icon-user"></i>
                <span class="title">个人信息维护</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="#/userProfileEdit">
                        更改用户信息</a>
                </li>
                <li>
                    <a href="#/userPasswordEdit">
                        修改密码</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;">
                <i class="icon-user"></i>
                <span class="title">用户管理</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="#/userManager">用户管理</a>
                </li>
            </ul>
        </li>
        <li class="heading">
            <h3 class="uppercase">更多</h3>
        </li>
        <li>
            <a href="javascript:;">
                <i class="icon-logout"></i>
                <span class="title">系统设置</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="#/systemLogger">
                        系统日志</a>
                </li>
            </ul>
        </li>
	</ul>
	<!-- END SIDEBAR MENU -->
</div>	