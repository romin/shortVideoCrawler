<!-- BEGIN PAGE HEADER-->
<#--<h3 class="page-title">-->
    <#--系统用户管理中心-->
<#--</h3>-->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#/home">主页</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#/userManager">用户管理</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-10">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>系统用户一览
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="userManagerTable">
                    <thead>
                    <tr>
                        <th>
                            用户名
                        </th>
                        <th>
                            用户昵称
                        </th>
                        <th>
                            手机
                        </th>
                        <th>
                            邮箱
                        </th>
                        <th>
                            角色
                        </th>
                        <th>状态</th>
                        <th class="col-sm-1">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                        <#list managers as manager>
                            <tr>
                                <td manager_id="${manager.id}">
                                    ${manager.userName}
                                </td>
                                <td>
                                    ${ manager.nickName!""}
                                </td>
                                <td>
                                    ${ manager.telephone!""}
                                </td>
                                <td>
                                    ${ manager.email !""}
                                </td>
                                <td>
                                    <#if manager.accessType==0>
                                    普通用户
                                    <#elseif manager.accessType==1>
                                    代理商
                                    <#elseif manager.accessType==2>
                                    管理员
                                    </#if>
                                </td>
                                <td>
                                ${ (!manager.status??||manager.status==0)?string("正常","屏蔽")}
                                </td>
                                <td>
                                <a class="edit" href="javascript:">编辑</a>
                                </td>
                            </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END MAIN CONTENT -->
<!-- BEGIN MAIN JS -->
<script>
    TableAdvanced.init();
</script>
<!-- END MAIN JS -->