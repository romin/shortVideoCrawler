<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>详情页</title>
</head>
<body>
<#if article.adsLocation!=1>
   <@ads article/>
</#if>
<div class="content">
    ${article.content}
</div>
<#if article.adsLocation==1>
    <@ads article/>
</#if>
</body>
</html>

<#macro ads article>
<div class="ads">
    <#if article.advertisement??>
        <a target="_blank" href="${article.advertisement.targetUrl}">
            <#if article.advertisement.desc??>
            ${article.advertisement.desc}
            <#elseif article.advertisement.image??>
                <img src="${article.advertisement.image}">
            </#if>
        </a>
    </#if>
</div>
</#macro>