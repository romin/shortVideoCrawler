<#--<h3 class="page-title">-->
    <#--文章编辑-->
<#--</h3>-->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#/home">主页</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#/userManager/">文章编辑</a>
        </li>
    </ul>
</div>

<div class="row" ng-controller="articleEdit">
    <div class="col-md-12">
        <!-- BEGIN EXTRAS PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>文章编辑
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal form-bordered">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-1">文章标题</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="title" ng-model="articls.title" id="article_title" value="{{articls.title}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1">内容</label>
                            <div class="col-md-10">
                                <div name="summernote" id="summernote_1" ng-model="articls.content" article="{{articls.content}}">
                                    {{articls.content}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-10">
                                <button type="submit" class="btn purple" ng-click="processSubmit()"><i class="fa fa-check"></i>提交</button>
                                <button type="button" class="btn default">取消</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        ComponentsEditors.init();
    })
</script>