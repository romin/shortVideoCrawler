<!-- BEGIN PAGE HEADER-->
<#--<h3 class="page-title">-->
    <#--系统用户管理中心-->
<#--</h3>-->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#/home">主页</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#/userManager">文章管理</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-10">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>文章管理
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a  class="btn green button" href="#/articleEdit/">
                                    新增 <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover" id="articleManagerTable">
                    <thead>
                    <tr>
                        <th>
                            标题
                        </th>
                        <th>
                            关联广告
                        </th>
                        <th>
                            广告位置
                        </th>
                        <th>
                            操作
                        </th>
                        <th>
                            预览
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <#list articles as article>
                            <tr>
                                <td manager_id="${article.id}">
                                    <a href="#/articleEdit/${article.id}">${article.title}</a>
                                </td>
                                <td>
                                    <#if article.advertisement??>
                                        ${article.advertisement.name}
                                    <#else>
                                        无
                                    </#if>
                                </td>
                                <td>
                                    <#if !(article.adsLocation??)||article.adsLocation==0>
                                        无
                                    <#elseif article.adsLocation==1>
                                        底部
                                     <#elseif  article.adsLocation==2>
                                        顶部
                                    </#if>
                                </td>
                                <td>
                                <a class="edit" href="javascript:">编辑</a>
                                </td>
                                <td>
                                    <a class="view" target="_blank" href="/admin/articleDetail/${article.id}">预览</a>
                                </td>
                            </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<select class="form-control input-sm advertisement_model hidden">
    <#list advertisements as ad>
        <option value="${ad.id}" val_dsc="${ad.name}">${ad.name}</option>
    </#list>
</select>
<!-- END MAIN CONTENT -->
<!-- BEGIN MAIN JS -->
<script>
    TableAdvanced.init();
</script>
<!-- END MAIN JS -->